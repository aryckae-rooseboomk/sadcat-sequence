# vue-sequence
A project by Aryckae and Rooseboomk made with Vue as a fun pair programming side project to repeat the basics of Vue.

## Functionality
This is a sequence game so at the start you'll only see dark boxes and a pop up to query the size of the field.
Afterwards one of the boxes will light up and you'll have to click it, this will repeat with an additional image untill a mistake is made in the sequence.

## Remark
This was a project made in the course of two afternoons, Functionality wise it works which was the focus of this project. There are some visual bugs, depending on what browser you use.
